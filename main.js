var list = document.querySelector('.list-group')
var edit = document.getElementsByClassName('edit')
var del = document.getElementsByClassName("delete")
const API_link = 'http://localhost:3000/toDoList'



function getTime() {
    let today = new Date()
    let date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear()
    let time = today.getHours() + "h:" + today.getMinutes() + "m:" + today.getSeconds() + 's'
    let dateTime = date + ' ' + time
    return dateTime
}

function getToDoList(callback) {
    fetch(API_link)
        .then(function (response) {
            return response.json()
        })
        .then(callback)
        .catch(function (err) {
            console.log(err)
        })
}

// in ra man hinh 
function update(toDoList) {
    list.innerHTML = ''
    console.log(toDoList)
    for (let i = 0; i < toDoList.length; i++) {
        list.insertAdjacentHTML('afterbegin',
            `
            <a href="#" class="list-group-item list-group-item-action d-flex justify-content-between" aria-current="true">
                <div class="d-flex flex-column justify-content-center">
                    <h5 class="mb-1 text-wrap item-title">${toDoList[i]["title"]}</h5>
                    <p class="mb-1 item-description">${toDoList[i]["description"]}</p>
                </div>
                <div class="d-flex flex-column">
                    <small class="text-end item-time">${toDoList[i]["time"]}</small>
                    <!-- button -->
                    <div class="d-flex">
                        <button type="button" onclick="editElement(this, ${toDoList[i]["id"]})" class="btn btn-outline-success me-1 edit">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path
                                    d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z">
                                </path>
                                <path fill-rule="evenodd"
                                    d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z">
                                </path>
                            </svg>
                            <span>Edit</span>
                        </button>
                        <button type="button" onclick="deleteElement(this, ${toDoList[i]["id"]})" class="btn btn-outline-danger delete">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-trash" viewBox="0 0 16 16">
                                <path
                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z">
                                </path>
                                <path fill-rule="evenodd"
                                    d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z">
                                </path>
                            </svg>
                            <span>Delete</span>
                        </button>
                    </div>
                </div>
            </a>
            `
        )
    }
}

// Create a new list item when clicking on the "Add" button
function newElement(event) {
    let inputTitle = document.getElementById("title").value
    let inputDescription = document.getElementById("description").value
    if (inputTitle === '') {
        alert("You must write title!")
    }
    else {
        let formData = {
            title: inputTitle,
            description: inputDescription,
            time: getTime()
        }

        let option = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
        }

        fetch(API_link, option)
            .then(function (response) {
                return response.json()
            })
            .then(update)
    }


    document.getElementById("title").value = ''
    document.getElementById("description").value = ''
    document.getElementById("title").focus()

}

function deleteElement(target, id) {
    let choice = confirm('Do you really want to delete it?')
    if (choice) {
        let item = target.parentElement.parentElement.parentElement
        let option = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            }
        }
        fetch(API_link + '/' + id, option)
            .then(function(response){
                response.json()
            })
            .then(function(){
                if(item){
                    item.remove()
                }
            })
    }

    
}

function editElement(target, id) {
    let item = target.parentElement.parentElement.parentElement
    if (target.getElementsByTagName('span')[0].innerHTML == 'Edit') {
        target.getElementsByTagName('span')[0].innerHTML = 'Done'
        let title = item.getElementsByClassName('item-title')[0]
        let inputTitle = document.createElement('input')
        inputTitle.type = 'text'
        inputTitle.className = 'form-control    item-input-title'
        inputTitle.value = title.innerHTML
        title.parentElement.replaceChild(inputTitle, title)

        let description = item.getElementsByClassName('item-description')[0]
        let inputDescription = document.createElement('input')
        inputDescription.type = 'text'
        inputDescription.className = 'form-control item-input-description'
        inputDescription.value = description.innerHTML
        description.parentElement.replaceChild(inputDescription, description)
    }
    else {

        let inputTitle = item.getElementsByClassName('item-input-title')[0]
        if (inputTitle.value === '')
            alert("You must write title!")
        else {
            let title = document.createElement('h5')
            title.className = 'mb-1 text-wrap item-title'
            title.innerHTML = inputTitle.value
            inputTitle.parentElement.replaceChild(title, inputTitle)

            let inputDescription = item.getElementsByClassName('item-input-description')[0]
            let description = document.createElement('p')
            description.className = 'mb-1 item-description'
            description.innerHTML = inputDescription.value
            inputDescription.parentElement.replaceChild(description, inputDescription)

            let formData = {
                title: inputTitle.value,
                description: inputDescription.value,
            }
    
            let option = {
                method: 'PATCH',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(formData)
            }
    
            fetch(API_link + '/' + id, option)
                .then(function (response) {
                    return response.json()
                })
                .then(update)

            target.getElementsByTagName('span')[0].innerHTML = 'Edit'
        }
    }
}

getToDoList(update)